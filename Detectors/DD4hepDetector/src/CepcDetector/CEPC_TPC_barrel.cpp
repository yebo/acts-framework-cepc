#include "Acts/Plugins/DD4hep/ActsExtension.hpp"
#include "Acts/Plugins/DD4hep/ConvertDD4hepMaterial.hpp"
#include "DD4hep/DetFactoryHelper.h"

using namespace std;
using namespace dd4hep;

/**
 Constructor for a cylindrical barrel volume, possibly containing layers and the
 layers possibly containing modules.
 */

static Ref_t
create_element(Detector& lcdd, xml_h xml, SensitiveDetector sens)
{
    xml_det_t x_det    = xml;
    string    det_name = x_det.nameStr();
    // Make DetElement
    DetElement cylinderVolume(det_name, x_det.id());
  
    // add Extension to Detlement for the RecoGeometry
    Acts::ActsExtension* detvolume = new Acts::ActsExtension();
	detvolume->addType("barrel", "detector");
    cylinderVolume.addExtension<Acts::ActsExtension>(detvolume);
  
    // make Volume
    dd4hep::xml::Dimension x_det_dim(x_det.dimensions());
    Tube   tube_shape(x_det_dim.rmin(), x_det_dim.rmax(), x_det_dim.dz());
    Volume tube_vol(det_name, tube_shape,lcdd.vacuum()); 
    double Length_TPC_=x_det_dim.dz();
    double Thickness_TPC_=x_det_dim.rmax()-x_det_dim.rmin();
    tube_vol.setVisAttributes(lcdd, x_det_dim.visStr());
  
    // go trough possible layers
    double layer_n_total;
    int layer_number_=0;
    for (xml_coll_t j(xml, _U(layer)); j; ++j) {
        xml_comp_t x_layer = j;
        double     TPC_rmin = x_layer.rmin();
        double     TPC_rmax = x_layer.rmax();
        string     layer_name_temp=x_layer.nameStr();
        double     TPC_layer_number=x_layer.number(); // get how many TPC layers you wanted to add 
        std::cout<<layer_name_temp<<" : "<<TPC_layer_number<<std::endl;  
        double     TPC_layer_thickness  = (TPC_rmax-TPC_rmin)/TPC_layer_number; // the thickness of the layer
        std::cout<<"thickness :"<<TPC_layer_thickness<<std::endl;
        int        has_thin_layer = x_layer.attr<int>(_Unicode(has_thin_layer)); // whether you wanted to add a thin layer instead of modules
  
  
        double l_rmin=TPC_rmin;
        double l_rmax=0.0; 
        string layer_name = layer_name_temp;
        Volume layer_vol(layer_name,Tube(TPC_rmin, TPC_rmax, Length_TPC_),lcdd.material(x_layer.materialStr()));
        DetElement lay_det(cylinderVolume, layer_name, layer_number_);
        layer_number_++;
        layer_vol.setVisAttributes(lcdd, x_layer.visStr());
        Acts::ActsExtension* detlayer = new Acts::ActsExtension();
	    detlayer->addType("active cylinder", "layer");
        lay_det.addExtension<Acts::ActsExtension>(detlayer);
        double module_num=0.0;
        int exter=1;
        for (int layer_v=0;layer_v<TPC_layer_number;layer_v++){
  
            // Create Volume for Layer
            //string layer_name = layer_name_temp+_toString((int)layer_v,"_layer_%d");
  
            //l_rmin +=TPC_layer_thickness;
            l_rmin =TPC_rmin+layer_v*TPC_layer_thickness+0.01;
            //std::cout<<"creating layer : "<<layer_v<<" at "<< l_rmin<<std::endl;
             
            l_rmax = l_rmin+TPC_layer_thickness-0.01;
  
            //get the informations from the modules
            if (x_layer.hasChild(_U(module))){
                double module_v=0;
                double scale_factor=1.0;
                has_thin_layer = 0; // if you define modules then the thin layer will not be added
         
                // calculate the size of the module automatically by defining the deltaphi
                                    // 2.5 degree : 0.0436 rad
                                    // 5 degree : 0.087rad 
                                   // 10 degree : 1.174rad
                for (xml_coll_t i(x_layer, _U(module));i;i++){
                    // get the module's config and put them in the right place
                    xml_comp_t x_module = i;
                    string module_name = layer_name+_toString((int)module_v,"_module_%d");
                    double module_thickness = x_module.thickness();
                    double module_width = x_module.width(); // width will be optimized
                    double module_length = x_module.length();
                   // double deltaphi=2.*acos(l_rmin/l_rmax);              
                 // double deltaphi=2.*atan((module_width)/((l_rmin+l_rmax)*0.5));
                    //double deltaphi=atan(module_width/l_rmin);
                    double deltaphi=2.0*atan(module_width/l_rmin);
                     
                   // module_width = l_rmax*sin(deltaphi);
                   // std::cout<<"module_width : "<<module_width<<std::endl;
                    int repeat = floor(2.0 * M_PI / deltaphi);
                    int zrepeat = floor(Length_TPC_/module_length);
                    deltaphi=2.0 * M_PI / repeat;
                    //repeat = floor(repeat *0.9); // scale down
  
                    // create the module volume and its corresponing component volumes first
                    
                    Volume mod_vol("module",Box(module_length, module_width*scale_factor, module_thickness),lcdd.material(x_module.materialStr()));
                    mod_vol.setVisAttributes(lcdd, x_module.visStr());
  
                    //std::cout<<"modules on R : "<<repeat<<std::endl;
                    //std::cout<<"modules on Z : "<<zrepeat<<std::endl;
                    if (1){   // only put one big snesitive area in Z direction
                        int m_z=0;
                        double phi = 0.0;
                        for (int m_r=0;m_r<repeat;m_r++){
                            phi = deltaphi/dd4hep::rad * m_r;
                            //std::cout<<"Adding module at Z : "<<m_z * module_length*2<<" phi : "<<phi<<std::endl; 
                            string module_name = layer_name+ _toString((int)module_num, "_module%d");
                          //  Position trans( 0.5*(l_rmin+l_rmax)* cos(phi), 0.5*(l_rmin+l_rmax) * sin(phi), m_z * module_length*2-(m_z/abs(m_z)*module_length));
                            Position trans( l_rmin* cos(phi),l_rmin * sin(phi), 0);
                            // create detector element
                            DetElement mod_det(lay_det, module_name, module_num);
                            if (x_module.isSensitive()){
                                mod_vol.setSensitiveDetector(sens);
                                Acts::ActsExtension* moduleExtension = new Acts::ActsExtension();
								moduleExtension->addType("module", "detector");
								moduleExtension->addType("axes", "definitions", "XZY"); 
                                mod_det.addExtension<Acts::ActsExtension>(moduleExtension);
                            }
                            // Place Module Box Volumes in layer
                            //PlacedVolume placedmodule = layer_vol.placeVolume(mod_vol,Transform3D(RotationX(-0.5 * M_PI) * RotationZ(-0.5 * M_PI) * RotationX(phi - 0.6 * M_PI),trans));
                            PlacedVolume placedmodule = layer_vol.placeVolume(mod_vol,Transform3D(RotationX(-0.5 * M_PI) * RotationZ(-0.5 * M_PI) * RotationX(phi - 0.5 * M_PI),trans));
                            placedmodule.addPhysVolID("module", module_num);
                            // assign module DetElement to the placed module volume
                            mod_det.setPlacement(placedmodule);
                            module_num++;
   
                            }
                        }
                    }
                }else{
                    if (has_thin_layer==1){
                        // if no module defined, and you want to add a cylinder layer of thickness 0.01*mm at same radius
                        //
                        //set the name of the thin layer
                        std::cout<<"layer_thin : "<<layer_v<<" at "<<l_rmin<< " length: "<<Length_TPC_-0.1<<std::endl;
                        string layer_thin_name = layer_name+ _toString((int)layer_v,"_sub_%d");
                        // create the volumen at l_rmin and fill the volume with vacuum
                        Volume layer_thin_vol(layer_thin_name,Tube(l_rmin,l_rmin+1.0,Length_TPC_-0.1),lcdd.vacuum()); // unit cm
                        // set vis
                        layer_thin_vol.setVisAttributes(lcdd, x_layer.visStr());
                        // creats Detector element
                        DetElement layer_thin_det(cylinderVolume, layer_thin_name, layer_v);
                        //DetElement layer_thin_det(lay_det, layer_thin_name, layer_v);
                        // set the volume to sensitive
                        layer_thin_vol.setSensitiveDetector(sens);
                        // config and add actsextension to the layer_thin_det
                        Acts::ActsExtension* layer_thinExtension = new Acts::ActsExtension();
						layer_thinExtension->addType("active cylinder", "layer");
						//layer_thinExtension->addType("sensitive_tpc", "detector");
                        layer_thin_det.addExtension<Acts::ActsExtension>(layer_thinExtension);
                        // place the volume default: no rotation, no shift
                        PlacedVolume placeLayer_thin = layer_vol.placeVolume(layer_thin_vol);
                        //placeLayer_thin.addPhysVolID("layer",layer_n_total);
                        placeLayer_thin.addPhysVolID("sub_layer",layer_v);
                        layer_thin_det.setPlacement(placeLayer_thin);
                        layer_n_total++;
                        }
                    }
            }
  
       // Acts::ActsExtension* detlayer = new Acts::ActsExtension();
	   // detlayer->addType("active cylinder", "layer");
       // lay_det.addExtension<Acts::ActsExtension>(detlayer);
        // Place layer volume
        PlacedVolume placedLayer = tube_vol.placeVolume(layer_vol);
        placedLayer.addPhysVolID("layer", layer_n_total);
        // Assign layer DetElement to layer volume
        lay_det.setPlacement(placedLayer);
        layer_n_total++;
        }
  
    // Place Volume
    Volume       mother_vol = lcdd.pickMotherVolume(cylinderVolume);
    PlacedVolume placedTube = mother_vol.placeVolume(tube_vol);
    placedTube.addPhysVolID("system", cylinderVolume.id());
    cylinderVolume.setPlacement(placedTube);
    return cylinderVolume;
}
DECLARE_DETELEMENT(cepc_TPC_barrel_cylinder, create_element)
